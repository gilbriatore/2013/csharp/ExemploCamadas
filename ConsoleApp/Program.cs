﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dados;
using Negocios;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            CodigoDeBarrasDAO dao = new CodigoDeBarrasDAO();
            List<CodigoDeBarras> lista = dao.ListaDeCodigos;
            
            foreach (CodigoDeBarras codigo in lista)
            {
                Console.WriteLine("Identificação do produto: {0} ", codigo.ProdutoId);    
            }

            //Parada.
            Console.ReadLine();
        }
    }
}
