﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocios
{
    public class CodigoDeBarras
    {

        private string produtoId;
        private string seguimentoId;
        private string valorReal;
        private string dv;
        private string valor;
        private string empresaOrgaoId;
        private string campoLivre1;
        private string cnpjMf;
        private string campoLivre2;

        //Método construtor padrão explícito.
        public CodigoDeBarras()
        {
        }

        public CodigoDeBarras(string linha)
        {
            //Conforme especificação no documento da Febraban.
            this.produtoId = linha.Substring(0, 1);
            this.seguimentoId = linha.Substring(1, 1);
            this.valorReal = linha.Substring(2, 1);
            this.dv = linha.Substring(3, 1);
            this.valor = linha.Substring(4, 11);
            this.empresaOrgaoId = linha.Substring(15, 4);
            this.campoLivre1 = linha.Substring(19, 25);
            this.cnpjMf = linha.Substring(15, 8);
            this.campoLivre2 = linha.Substring(23, 21);
        }

        

        public string CampoLivre2
        {
            get { return campoLivre2; }
            set { campoLivre2 = value; }
        }
        

        public string CnpjMf
        {
            get { return cnpjMf; }
            set { cnpjMf = value; }
        }


        public string CampoLivre1
        {
            get { return campoLivre1; }
            set { campoLivre1 = value; }
        }
        


        public string EmpresaOrgaoId
        {
            get { return empresaOrgaoId; }
            set { empresaOrgaoId = value; }
        }
        


        public string Valor
        {
            get { return valor; }
            set { valor = value; }
        }


        public string Dv
        {
            get { return dv; }
            set { dv = value; }
        }
        
        
        public string ValorReal
        {
            get { return valorReal; }
            set { valorReal = value; }
        }
        


        public string ProdutoId
        {
            get { return produtoId; }
            set { produtoId = value; }
        }
        

        public string SeguimentoId
        {
            get { return seguimentoId; }
            set { seguimentoId = value; }
        }
        




        /*
        public string ProdutoId
        {
            get { 
                //codigo    
                return produtoId; 
            }
            set { 
                //Validacoes
                produtoId = value; 
            }
        }

        //Define o valor
        public void SetProdutoId(string produtoId)
        {
            this.produtoId = produtoId;
        }

        //Retorna o valor
        public string GetProdutoId()
        {
            return produtoId;
        }
         */
    }

    /*
    class Teste
    {
        static void Main(string[] args)
        {
            CodigoDeBarras codigo = new CodigoDeBarras();

            codigo.SetProdutoId("valor");
            string v = codigo.GetProdutoId();


            codigo.ProdutoId = "valor";
            string v2 = codigo.ProdutoId;




        }


    }
     */
}
