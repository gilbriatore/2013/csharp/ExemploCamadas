﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Negocios;

namespace Dados
{
    public class CodigoDeBarrasDAO
    {
        private List<CodigoDeBarras> listaDeCodigos = new List<CodigoDeBarras>();
        public List<CodigoDeBarras> ListaDeCodigos
        {
            get { return listaDeCodigos; }
            set { listaDeCodigos = value; }
        }

        public CodigoDeBarrasDAO()
        {
            string[] linhas = File.ReadAllLines("arquivo.txt");            
            foreach (var linha in linhas)
            {
                CodigoDeBarras codigo = new CodigoDeBarras(linha);
                listaDeCodigos.Add(codigo);                
            }
        }
    }
}
